-- Création de la base de données
CREATE DATABASE clublambda;

-- Utilisons cette nouvelle base de données
USE clublambda;

-- Création d'un utilisateur admin 
-- et on lui donne tous les droits sur la base de données
-- et sur toutes les tables de la base
CREATE USER 'adminClub'@'%' IDENTIFIED BY 'H0u2ard9o';
GRANT ALL PRIVILEGES ON clublambda.* TO 'adminClub'@'%';


s