<?php
/**
 * Classe de connexion à la base de donnée
 */
include_once('Seance.php');
include_once('User.php');

 class Database{

    // Constantes de connxion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "H0u2ard9o";

    // Attribut de la classe
    private $connexion;

    // Constructeur pour initier la connexion
    public function __construct(){
        try {
            $this->connexion = new
            PDO("mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=". self::DB_NAME.";charset=UTF8",
                                        self::DB_USER,
                                        self::DB_PASSWORD);
        }catch (PDOException $e){
            echo 'Connexion échouée : ' . $e->getMessage();
        }
    }
    
    /**
     * Fonction pour créer une nouvelle séance en base de données
     * 
     * @param{Seance} seance : la séance à sauvegarder
     * 
     * @return{integer, boolean} l'id si la séance à été créée ou false sinon
      */
      public function createSeance(Seance $seance){
        
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances (titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
            VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        // J'execute ma requête en passant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"     => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut"        => $seance->getHeureDebut(),
            "date"              => $seance->getDate(),
            "duree"             => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur"           => $seance->getCouleur()
        ]);
        // Je recupère l'id créé si l'execution s'est bien passée )code 0000 de MySQL
        if($pdoStatement->errorCode() == 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else{
            return false;
        }
    }  
    /**
     * Cette fonction cherche la seance dont l'id est passé en paramètre
     * et la retourne
     * 
     * @param{integer} id : l'id de la séance recherchée
     * 
     * @return{Seance|boolean} : Un objet Seance si la séance a été trouvée, false sinon
     */
    public function getSeanceById($id){
        // Je preépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        // J'execute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id" => $id]
        );
        // j'execute le résultat
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }
    /**
     * -----------------------------------------------------------------------
     * Fonction qui retourne toutes les séances de la semaine
     * 
     * @param{integer} week : le numéro de la semaine recherchée
     * 
     * @return{array} : un tableau de Seance s'il y a des séances programmées pour cette semaine
     */
    public function getSeanceByWeek($week){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE WEEKOFYEAR(date) = :week"
        );
        // J'exécute la requête en lui passant le numéro de la semaine
        $pdoStatement->execute(["week"=> $week]
        );
        // Je recupere les resulats 
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        return $seances;
    }
    // ---------------------------------------------------------------------- P45
    
    public function deleteAllSeance(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        );
        $pdoStatement->execute();
    }
    
    /**
    * Fonction pour mettre à jours une scéance en base de données
    * 
    * @param{Seance} seance : la scéance est à jour
    * 
    * @return(booleen) true si la séance est mis à jour ou false sinon
    * 
    */
    public function updateSeance(Seance $seance){
        // Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "UPDATE seances
            SET titre = :titre, description = :description, heureDebut = :heureDebut, 
            date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax, couleur = :couleur
            WHERE id = :id"
        );
        // J'excute ma requête en passant les valeurs de l'objet Seance en objet
        $pdoStatement->execute([
            "titre"             =>$seance->getTitre(),
            "description"       =>$seance->getDescription(),
            "heureDebut"        =>$seance->getHeureDebut(),
            "date"              =>$seance->getDate(),
            "duree"             =>$seance->getDuree(),
            "nbParticipantsMax" =>$seance->getNbParticipantsMax(),
            "couleur"           =>$seance->getCouleur(),
            "id"                =>$seance->getId()
        ]);

        // Retourne true créé si l'execution s'est bien passée (code 0000 de my SQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }


    /**
    * Fonction pour supprimer une séance en base de donnée
    * @parametre{integer} id : l'id de la séance à supprimer
    * @return{booleen}
    */

    public function deleteSeance($id){
        // le prepare la requette pour supprimer tous les inscrits à la séance
        $pdoStatement = $this->connexion->prepare(
            "Delete FROM inscrits WHERE id_seance = :seance"
        );
        // j'execute la seance
        $pdoStatement->execute(
            ["seance" => $id]
        );
        // retourne true
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return  false;
        }
    }

    public function insertParticipant($idUser){
        // Je prépare la requête d'insertion
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO inscrits (id_user, id_seance) VALUES (:id_user, id_sceance)"
        );
        // j'execute ma scéance
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance => $idSeance"]
        );
        // je retourne true su exe est bien paséé (cd 0000 de mysql)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }
    // -------------p48
    public function deleteParticipant($idSeance, $idUser){
        // Je prepare la requete d'insertion
        $pdoStatement = $this->connection->prepare(
            "DELETE FROM inscrits WHERE id_user = : id_user AND id_seance = : id_seance"
        );
        //j'execute ma requete
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance => $idSeance"]
        );
         // retourne true si execute pas o codeoooo de mysql
         if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    /** Fonction pour créée un nouveau user en base de données
    *
    *@param{User} : le user a sauvegarder
    *
    *@param{interger, booleen} l'id si le user cree a ete false sinon
    */
    public function CreateUser(User $user){
        // je prepaere ma requete SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO users(nom, email, password, isAdmin, isActif, token)
            VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
        );
        // J'execute ma requete en passant les val de l'objet User en valeur
        $pdoStatement->execute([
            "nom"         =>$user->getNom(),
            "email"       =>$user->getEmail(),
            "password"    =>$user->getPassword(),
            "isAdmin"     =>$user->isAdmin(),
            "isActif"     =>$user->isActif(),
            "token"       =>$user->getToken()
        ]);
        // Je recpere l'id vréé si exe est ok 0000 de mysql
        if($pdoStatement->errorCode() == 0){
            $is = $this->connexion->éastInsertId();
            return true;
        }else{
            return false;
        }
    }
        public function deleteAllUser(){
            //je prep ma requete
            $pdoStatement = $this->connexion->prepare(
                "DELETE FROM users;"
            );
            $pdoStatement->execute();
        }
        public function deleteAllInscrit(){
            //je prep ma requete
            $pdoStatement = $this->connexion->prepare(
                "DELETE FROM inscrits;"
            );
            $pdoStatement->execute();
        }

        public static function tearDowmAfterClass(){
            $database = new Database();
            $database->deleteAllInscrit();
            $database->deleteAllUser();
            $database->deleteAllSeance();
        }
    }







?>