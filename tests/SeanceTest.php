<?php
// Ceci remplace l'instruction quand on defini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__ ."/../models/Seance.php");
include_once(__DIR__ ."/../models/User.php");
include_once(__DIR__ ."/../models/Database.php");

final class SeanceTest extends TestCase{

    public function testCreateSeance(){
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

        $database = new Database();

        $this->assertNotFalse($database->createSeance($seance));        
    }

    public function testGetSeanceById(){
        $database = new Database();
        // Je crée et j'insère une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc"); 
        // Je recupère l'id de la séance
        $id = $database->createSeance($seance);
        // Je certifie que la séance existe
        $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));
    }

    // ------------------------------
    public function testGetSeanceByWeek(){
        $database = new Database();
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        $this->assertNotFalse($database->createSeance($seance));
        $nbSeances = count($database->getSeanceByWeek(date("W")));
        echo($nbSeances);
        $this->assertGreaterThan(0, $nbSeances);
    }
    // ------------------------------P46
    public static function tearDownAfterClass(){
        $database = new Database();
        $database->deleteAllSeance();
    }
    // ------------------------------P46
    public function testUpdateSeance(){
        $database = new Database();
        // Je créé une seance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // je recurère l'id
        $id = $database->createSeance($seance);
        // Je recupère la séance en base de données
        $seance->setTitre("Yoga");
        // je verifie la mise à jours de la séance  
    $this->assertTrue($database->upDateSeance($seance));
    }
    // ------------------------------P47
    public function testDeleteSeance(){
        $database = new Database();
        // Je créé une seance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // je recurère l'id
        $id = $database->createSeance($seance);
        // je supprime la séance  
    $this->assertTrue($database->deleteSeance($id));
    }

}

?>