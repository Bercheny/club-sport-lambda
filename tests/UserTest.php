<?php
// Ceci remplace l'instruction quand on defini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__ ."/../models/User.php");
include_once(__DIR__ ."/../models/Seance.php");
include_once(__DIR__ ."/../models/Database.php");

final class Usertest extends TestCase{

    public function testCreateUser(){
        $user = User::createUser("Toto", "toto@gmail.com", password hash("1234", PASSWORD_DEFAULT),
                                        0, 0, bin2hex(random_bytes(20)));
        $database = new Database();

        $this->assertNotFalse($database->createUser($user));        
    }

    public function testInsertDeletParticipant(){
        // je cree un user
        $user::createUser("Toto", "toto@gmail.com", password hash("1234", PASSWORD_DEFAULT),
        0, 0, bin2hex(random_bytes(20)));
        // Sauvegarde
        $idUser = $database->createUser($user);
        $this->assertNotFalse($iduser);
        //creation seance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
            // je recurère l'id
            $idSeance = $database->CreateSeance($seance);
            $this->asserNotFalse($idseance);
            // insert participant
            $assertTrue($database->insertParticipant)$idSeance, $idUser));
            // je delete participant
            $assertTrue($database->deleteParticipant)$idSeance, $idUser));  
    }
}
?>