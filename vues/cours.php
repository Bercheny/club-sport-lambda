<?php   
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
   <h1 class="card-header">Pilates</h1>
   <div class="card-body text-left" style="background: #03bafc">
        <div class="row">
            <div class="offset-3 col-9">
                <p>Date : 10/01/2019</P>
            </div>
            <div class="offset-3 col-9">
                <p>Heure de début : 09h00</P>
            </div>
            <div class="offset-3 col-9">
                <p>Durée : 50 minutes</P>
            </div>
            <div class="offset-3 col-9">
                <p>Description :</P>
            </div>
            <div class="offset-3 col-9">
                <p>La méthode Pilates, parfois simplement appelée Pilates, est un système d'activité physique développé au début du XXe siècle par un passionné de sport et du corps humain, Joseph Pilates. 
                La méthode Pilates est pratiquée au tapis avec ou sans accessoires ou à l'aide d'appareils.
                Elle a pour objectif le développement des muscles profonds, l'amélioration de la posture, l'équilibrage musculaire et l'assouplissement articulaire, pour un entretien, une amélioration ou une restauration des fonctions physiques. </P>
            </div>
            <div class="offset-3 col-9">
                <p>Nombre de participants max : 20</P>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                    <a class="btn btn-danger" href="#">Se désinscrire</a>
                    <a class="btn btn-primary" href="#">S'inscrire</a>
                    <a class="btn btn-danger" href="#">Complet</a>
                </div>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                    <a class="btn btn-warning" href="#">Dupliquer</a>
                    <a class="btn btn-primary" href="#">Modifier</a>
                    <a class="btn btn-danger" href="#">Supprimer</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    include('modules/partie3.php')
?>