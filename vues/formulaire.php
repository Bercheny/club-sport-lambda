<?php
    include('modules/partie1.php');
?>

<div class="container card text center mt-4">
    <h1 class="card-header">Modifier Pilates</h1>
    <div class="card-body">
        <form class="text-left text-md-right" action="" method="POST">
            <div class="form-group row">
                <label for="nom" class="col-sm-12 col-md-4 col-form-label">Nom</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="nom" name="nom" 
                    placeholder="Nom du cours" value="Pilates" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="couleur" class="col-sm-12 col-md-4 col-form-label">Couleur de fond</label>
                <div class="col-sm-12 col-md-8">
                    <input type="color" class="form-control" id="couleur" name="couleur" 
                    placeholder="Couleur" value="#03bafc" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="date" class="col-sm-12 col-md-4 col-form-label">Date</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="date" name="date" value="2019-01-10" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="heureDebut" class="col-sm-12 col-md-4 col-form-label">Heure de début</label>
                <div class="col-sm-12 col-md-8">
                    <input type="time" class="form-control" id="heureDebut" name="heureDebut" value="09:00" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="duree" class="col-sm-12 col-md-4 col-form-label">Durée</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="duree" name="duree" value="50" required><span>minutes</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="descrition" class="col-sm-12 col-md-4 col-form-label">Description</label>
                <div class="col-sm-12 col-md-8">
                    <textarea class="col-sm-12 col-md-8" id="description" name="descrition">
                    Le CrossFit est une marque commerciale d'entraînement croisé promu par la société CrossFit Inc. 
                    Dans le langage courant, le terme CrossFit est assimilé à la pratique sportive pluridisciplinaire suivant les principes énoncés par la marque éponyme.
                    Le CrossFit combine principalement la force athlétique, l'haltérophilie, la gymnastique et les sports d'endurance1. Le mot crossfit vient de la contraction de cross fitness (en français, entraînement croisé), appelé ainsi parce qu'il mélange différentes activités physiques et sportives préexistantes2.
                    Les pratiquants du CrossFit courent, rament, grimpent à la corde, sautent, déplacent des objets, pratiquent des mouvements olympiques d'haltérophilie ainsi que des exercices au poids du corps, utilisent des haltères (réglables ou girevoys), des anneaux de gymnastique, des boîtes, des sacs de sable et tout autre objet pouvant servir de poids.
                    Le CrossFit axe son fonctionnement autour de dix compétences athlétiques : endurance cardiovasculaire et respiratoire, endurance musculaire, force, souplesse, puissance, vitesse, agilité, psychomotricité, équilibre et précision.
                    Le programme CrossFit veut augmenter la capacité de travail dans ces différents domaines en provoquant par les entraînements des adaptations neurologiques et hormonales au travers des différentes filières métaboliques3,4. Ceci afin de préparer ses pratiquants à s’adapter à n’importe quels efforts physiques rencontrés tous les jours grâce à la variété des entraînements, l’utilisation de mouvements poly-articulaires et l’intensité élevée du travail
                    </textarea>. 
                </div>
            </div>
            <div class="form-group row">
                <label for="nbParticipants" class="col-sm-12 col-md-4 col-form-label">Nombre de participants max</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="nbParticipants" name="nbParticipants" value="20" required><span>participants</span>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">Modifier</button>
            </div> 
        </form>
    </div>
</div>

<?php
    include('modules/partie3.php')
?>



