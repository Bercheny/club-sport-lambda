<?php   
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue chez Lambda</h1>
    <div class="card-body">
        <img class="mainImage" src="/vues/assets/images/parcours-du-combattant_imagelarge.jpg">
        <p class="p-4">
        <b>Sport de cohésion</b>
        Le niveau de cohésion sociale permet de favoriser les synergies des organisations et la qualité de vie des membres des sociétés, 
        si les relations sociales sont vécues positivement par les individus constituant cette organisation ou cette société. 
        Mais elle peut aussi nuire à l'innovation, par sa tendance à créer des liens forts entre les membres et peu de liens avec l'extérieur; 
        une cohésion moindre, laissant place à des "trous structuraux" favorise l'innovation. 
        Une trop forte cohésion sociale peut aussi produire un repliement et maintenir davantage la ségrégation des rôles genrés.
        Prononcé pour la première fois par le sociologue Émile Durkheim dans son ouvrage De la division du travail social en 1893, 
        la cohésion sociale est alors l'état de bon fonctionnement de la société où s'exprime la solidarité entre individus et la conscience collective :
        « Nous sommes ainsi conduits à reconnaître une nouvelle raison qui fait de la division du travail une source de cohésion sociale. 
        Elle ne rend pas seulement les individus solidaires, comme nous l'avons dit jusqu'ici, parce qu'elle limite l'activité de chacun, 
        mais encore parce qu'elle l'augmente. Elle accroît l'unité de l'organisme, par cela seul qu'elle en accroît la vie; du moins, à l'état normal, 
        elle ne produit pas un de ces effets sans l'autre.
        </p>
        <a class="btn btn-dark" href="planning.php">Consulter le planning</a>
    </div>
</div>

<?php
    include('modules/partie3.php')
?>

